package java3finalexam_1533920.betgameclient;

import java3finalexam_1533920.betgameserver.BetGame;
import javafx.application.*;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.*;
import javafx.stage.*;

/**
 * This class grabs all the necessary Objects in order to create a GUI of BetGame
 * @author Juan-Carlos Sreng-Flores
 *
 */
public class BetGameGroup extends Group{
	private TextField betInput;
	private Text inputMessage;
	private Text score;
	private Button tails;
	private Button heads;
	private BetGame betGame;
	/**
	 * Constructor for BetGroup where the elements of BetGroup are encapsupalted inside this class.
	 */
	public BetGameGroup() {
		//Setup VBox inside this Group Object.
		initialize();
		VBox finalFrame = setupFrameWork();
		eventHandlers();
		//insert the VBox in the Object and ready to display.
		this.getChildren().add(finalFrame);
	}
	private void initialize() {
		betInput = new TextField();
		inputMessage  = new Text();
		score = new Text();
		inputMessage.setText("Place your bet here!!");
		tails = new Button("TAILS");
		heads = new Button("HEADS");
		betGame = new BetGame();
		score.setText("Your score is: "+betGame.getCurrentMoney());
	}
	private VBox setupFrameWork() {
		HBox buttons = new HBox();
		buttons.getChildren().addAll(tails, heads);
		VBox inputMessageAndBetInputAndButtons = new VBox();
		inputMessageAndBetInputAndButtons.getChildren().addAll(inputMessage, betInput, buttons, score);
		return inputMessageAndBetInputAndButtons;
	}
	private void eventHandlers() {
		tails.setOnAction(new EventHandler<ActionEvent>() {
			/**
			 * Action event for the buttons actions
			 * @param arg0 ActionEvent type obect
			 */
			@Override
			public void handle(ActionEvent arg0) {
				try {
					if(betInput.getText() != "") {
						int betAmount = Integer.parseInt(betInput.getText());
						inputMessage.setText(betGame.tryABet(betAmount, BetGame.Coin.Tails));
						score.setText("Your score is: "+betGame.getCurrentMoney());
					}else {
						throw new IllegalArgumentException();
					}
				}
				catch(Exception e) {
					inputMessage.setText("It is not a number!");
					score.setText("Your score is: "+betGame.getCurrentMoney());
				}
			}
			
		});
		heads.setOnAction(new EventHandler<ActionEvent>() {
			/**
			 * Action event for the buttons actions
			 * @param arg0 ActionEvent type obect
			 */
			@Override
			public void handle(ActionEvent arg0) {
				try {
					if(betInput.getText() != "") {
						int betAmount = Integer.parseInt(betInput.getText());
						inputMessage.setText(betGame.tryABet(betAmount, BetGame.Coin.Tails));
						score.setText("Your score is: "+betGame.getCurrentMoney());
					}else {
						throw new IllegalArgumentException();
					}
				}
				catch(Exception e) {
					inputMessage.setText("It is not a number!");
					score.setText("Your score is: "+betGame.getCurrentMoney());
				}
			}
			
		});
	}
	
}
