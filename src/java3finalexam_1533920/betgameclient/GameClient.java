package java3finalexam_1533920.betgameclient;


import javafx.application.*;
import javafx.scene.*;
import javafx.scene.control.*;
import javafx.scene.layout.*;
import javafx.scene.paint.Color;
import javafx.scene.text.*;
import javafx.stage.*;

/**
 * This class launches the Game Client as a GUI.
 * @author Juan-Carlos Sreng-Flores
 *
 */
public class GameClient extends Application{
	/**
	 * Main method.
	 * @param agrs
	 */
	public static void main(String[] agrs) {
		Application.launch(agrs);
	}
	/**
	 * @param stage - type Stage where it is used to launch the application GUI.
	 */
	@Override
	public void start(Stage stage) throws Exception {
		Group root = new Group();
		BetGameGroup betGroup = new BetGameGroup();
		
		root.getChildren().add(betGroup);
		Scene scene = new Scene(root, 650, 300); 
		scene.setFill(Color.WHITE);

		stage.setTitle("BetClient");
		stage.setScene(scene); 
		
		stage.show(); 
	}
}
