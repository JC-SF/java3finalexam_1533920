package java3finalexam_1533920.betgameserver;

import java.util.Random;

/**
 * This class is a BetGame Game where a user flips a Coin and wins or loses money accordingly.
 * @author JC-SF
 *
 */
public class BetGame {
	private Random rand;
	private int currentMoney;
	/**
	 * Constructor method, initialized the necessary private fields for it.
	 */
	public BetGame() {
		rand = new Random();
		currentMoney = 100;
	}
	/**
	 * This enum is created in order to simulate a Coin flipping value.
	 * @author JC-SF
	 *
	 */
	public enum Coin{
		Tails, Heads
	}
	/**
	 * This metod randomly generates a flipping coin for the user who is playing the game.
	 * @return Coin enum type.
	 */
	private Coin flip() {
		switch (rand.nextInt(2)) {
		case 0:
			return Coin.Tails;
		case 1:
			return Coin.Heads;
		default: 
			throw new IllegalArgumentException("Flip method is corrupted.");
		}
	}
	/**
	 * 
	 * @param betAmount - int the amount the user wants to bet.
	 * @param bet Coin - the side of the coin the user wants to bet on. 
	 * @return String - message received after winning/losing the bet.
	 */
	public String tryABet(int betAmount, Coin bet) {
		String message;
		if(betAmount <= currentMoney) {
			Coin flip = flip();
			if(flip == bet) {
				currentMoney+=betAmount;
				message = "You just won your bet on "+bet.toString()+" and you won "+betAmount;
			}
			else {
				currentMoney -= betAmount;
				message = "Aww! It flipped to "+flip.toString()+" and you lost "+betAmount;
			}
		}
		else {
			message = "Your betting amount is way out of your capacity, make sure it's less than you can afford!";
		}
		
		return message;
	}
	/**
	 * getter for currentMoney of the game.
	 * @return int - the current Money of the game.
	 */
	public int getCurrentMoney() {
		return currentMoney;
	}
}
