package java3finalexam_1533920.recursion;

public class Recursion {
	public static void main(String[] args) {
		int[] numbers = new int[] {1,2,3,4,5,6,7,8,9, 10, 11, 0, 11, 0, 11};
		System.out.println(recursiveCount(numbers, 2));
	}
	/**
	 * This method counts the number of elements less than 10, and at greater or equal to n.
	 * @param numbers
	 * @param n
	 * @return int - count of numbers at index n or greater less than 10. 
	 */
	public static int recursiveCount(int[] numbers, int n) {
		if(n < numbers.length) {
			int count = 0;
			if(n%2==0 && numbers[n] < 10) {
				count = 1;
			}
			n++;
			return count + recursiveCount(numbers, n);
		}
		return 0;
	}
}
